const express = require('express');
const expressWs = require('express-ws');
const GameServer = require('./game-server');

// Create express app
const restApp = express();
expressWs(restApp);

// Setup static route
restApp.use('/', express.static(__dirname + '/public'));

// Create game server and attach it to express app
new GameServer(restApp);

// Start server
restApp.listen(
    3000,
    () => console.log('JSnake server started on http://localhost:3000')
);
