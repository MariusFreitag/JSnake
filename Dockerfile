FROM node:8

EXPOSE 3000

# Specify working directory
WORKDIR /app

# Install npm dependencies
COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm install

# Copy project files
COPY . .

CMD ["npm", "start"]
