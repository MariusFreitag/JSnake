'use strict';

const User = require('./user.js');
const Snake = require('./snake.js');
const Rectangle = require('./rectangle.js');
const c = require('./constants.js');
const fs = require('fs');
const path = require('path');

class GameServer {

    constructor(expressApp) {
        this.users = [];
        this.collectibles = [];

        // Add and setup user on incoming connection
        expressApp.ws('/ws', function (ws, req) {
            let newId = this.getNextId();
            new User(newId, GameServer.getRandomSnake(newId), ws, this);
        }.bind(this));

        this.gameLoop();
    }

    // Adds a random collectible
    addRandomCollectible() {
        let x = Math.floor(Math.random() * (c.CANVAS_WIDTH - c.SNAKE_SIZE * 2)) + c.SNAKE_SIZE;
        let y = Math.floor(Math.random() * (c.CANVAS_HEIGHT - c.SNAKE_SIZE * 2)) + c.SNAKE_SIZE;

        x -= (x % (c.SNAKE_SIZE) * 1.25);
        y -= (y % (c.SNAKE_SIZE) * 1.25);

        this.collectibles.push(new Rectangle(x, y, c.SNAKE_SIZE / 2, c.SNAKE_SIZE / 2, GameServer.getRandomColor()));
    }

    getNextId() {
        if (this.users.length === 0) {
            return 1;
        }

        let usedIds = this.users.map(function (user) {
            return user.id;
        });

        for (let i = 1; true; i++) {
            if (!usedIds.includes(i)) {
                return i;
            }
        }
    }

    // Returns a random color
    static getRandomColor() {
        let colors = ['#F44336', '#E91E63', '#9C27B0', '#3F51B5', '#2196F3', '#009688', '#FF9800', '#FFC107', '#795548'];
        return colors[Math.floor(Math.random() * colors.length)];
    }

    // Creates a snake at a random position
    static getRandomSnake(userId) {
        let x = Math.floor(Math.random() * (c.CANVAS_WIDTH - c.SNAKE_SIZE * 2)) + c.SNAKE_SIZE;
        let y = Math.floor(Math.random() * (c.CANVAS_HEIGHT - c.SNAKE_SIZE * 2)) + c.SNAKE_SIZE;

        x -= (x % c.SNAKE_SIZE);
        y -= (y % c.SNAKE_SIZE);

        let playerColors = ['red', 'blue', 'teal', 'purple', 'orange', 'brown', 'green', 'pink'];

        return new Snake(x, y, 'grey', playerColors[userId % playerColors.length]);
    }

    gameLoop() {
        let updateData = {
            canvas: {
                width: c.CANVAS_WIDTH,
                height: c.CANVAS_HEIGHT,
                color: '#212121'
            },
            users: [],
            collectibles: []
        };

        while (this.collectibles.length < c.COLLECTIBLES_COUNT) {
            this.addRandomCollectible();
        }

        let canvas = new Rectangle(0, 0, c.CANVAS_WIDTH, c.CANVAS_HEIGHT);

        let usersToLoose = [];
        for (let i = 0; i < this.users.length; i++) {
            let currentUser = this.users[i];

            if (currentUser !== undefined && !currentUser.lost) {
                currentUser.snake.move();

                // Self collision, game boundary collisions
                if (currentUser.snake.collidesSelf() || !currentUser.snake.rectangles[0].isIn(canvas)) {
                    usersToLoose.push(currentUser);
                }
                // Collision with other snake
                for (let j = 0; j < this.users.length; j++) {
                    if (currentUser.id !== this.users[j].id) {
                        if (currentUser.snake.collidesWithOther(this.users[j].snake)) {
                            usersToLoose.push(currentUser);
                        }
                    }
                }

                if (usersToLoose.includes(currentUser)) {
                    continue;
                }

                // Look for collectible collisions
                for (let j = 0; j < this.collectibles.length; j++) {
                    if (currentUser.snake.rectangles[0].doesCollide(this.collectibles[j])) {
                        currentUser.collectibleCollision(this.collectibles[j]);
                        this.collectibles.splice(j, 1);
                    }
                }

                // Push user data
                updateData.users.push({
                    id: currentUser.id,
                    score: currentUser.score,
                    colorStreak: currentUser.colorStreak,
                    snake: currentUser.snake.getObject()
                });
            }
        }
        usersToLoose.forEach(function (user) {
            user.loose();
        });

        // Push all collectibles
        for (let i = 0; i < this.collectibles.length; i++) {
            updateData.collectibles.push(
                this.collectibles[i].getObject()
            );
        }

        // Send Update
        this.users.forEach(function (user) {
            if (user.socket.readyState === user.socket.OPEN) {
                user.socket.send(JSON.stringify({ updateData: updateData }));
            }
        });

        // Restart loop
        setTimeout(function () {
            this.gameLoop();
        }.bind(this), 1000 / 15);
    }
}

module.exports = GameServer;
