'use strict';

class Rectangle {
    constructor(x, y, width, height, fillColor, strokeColor, strokeWidth) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.fillColor = fillColor === undefined ? 'white' : fillColor;
        this.strokeColor = strokeColor === undefined ? fillColor : strokeColor;
        this.strokeWidth = strokeWidth === undefined ? 0 : strokeWidth;
    }

    getBounds() {
        return {
            leftX: this.x,
            rightX: this.x + this.width,
            leftY: this.y,
            rightY: this.y + this.height
        };
    }

    isOnSamePosition(rectangle) {
        return this.x === rectangle.x && this.y === rectangle.y && this.width === rectangle.width && this.height === rectangle.height;
    }

    doesCollide(rectangle) {
        let thisBounds = this.getBounds();
        let thatBounds = rectangle.getBounds();

        return (thisBounds.rightX > thatBounds.leftX && thisBounds.leftX < thatBounds.rightX) &&
            (thisBounds.rightY > thatBounds.leftY && thisBounds.leftY < thatBounds.rightY);
    }

    isIn(rectangle) {
        let thisBounds = this.getBounds();
        let thatBounds = rectangle.getBounds();

        return (thisBounds.leftX >= thatBounds.leftX && thisBounds.rightX <= thatBounds.rightX) &&
            (thisBounds.leftY >= thatBounds.leftY && thisBounds.rightY <= thatBounds.rightY);
    }

    getObject() {
        return {
            x: this.x,
            y: this.y,
            width: this.width,
            height: this.height,
            fillColor: this.fillColor,
            strokeColor: this.strokeColor,
            strokeWidth: this.strokeWidth
        };
    }
}

module.exports = Rectangle;