'use strict';

class Rectangle {
    constructor(x, y, width, height, fillColor, strokeColor, strokeWidth) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.fillColor = fillColor === undefined ? 'white' : fillColor;
        this.strokeColor = strokeColor === undefined ? fillColor : strokeColor;
        this.strokeWidth = strokeWidth === undefined ? 0 : strokeWidth;
    }

    getBounds() {
        return {
            leftX: this.x,
            rightX: this.x + this.width,
            upperY: this.y,
            lowerY: this.y + this.height
        };
    }

    isOnSamePosition(rectangle) {
        return this.x === rectangle.x && this.y === rectangle.y && this.width === rectangle.width && this.height === rectangle.height;
    }

    doesCollide(rectangle) {
        let thisBounds = this.getBounds();
        let thatBounds = rectangle.getBounds();

        return (thisBounds.rightX > thatBounds.leftX && thisBounds.leftX < thatBounds.rightX) &&
            (thisBounds.lowerY > thatBounds.upperY && thisBounds.upperY < thatBounds.lowerY);
    }

    isIn(rectangle) {
        let thisBounds = this.getBounds();
        let thatBounds = rectangle.getBounds();

        return (thisBounds.leftX >= thatBounds.leftX && thisBounds.rightX <= thatBounds.rightX) &&
            (thisBounds.upperY >= thatBounds.upperY && thisBounds.lowerY <= thatBounds.lowerY);
    }

    isAbove(rectangle) {
        let thisBounds = this.getBounds();
        let thatBounds = rectangle.getBounds();

        return (thisBounds.lowerY === thatBounds.upperY - 1) &&
            (thisBounds.rightX > thatBounds.leftX && thisBounds.leftX < thatBounds.rightX);

    }

    draw(gamelib) {
        if (gamelib instanceof GameLib) {
            gamelib.drawRectangle(this.x, this.y, this.width, this.height, this.fillColor, this.strokeColor, this.strokeWidth);
        }
    }

    getObject() {
        return {
            x: this.x,
            y: this.y,
            width: this.width,
            height: this.height,
            fillColor: this.fillColor,
            strokeColor: this.strokeColor,
            strokeWidth: this.strokeWidth
        };
    }
}

class GameLib {
    constructor(canvasName) {
        this.canvas = document.getElementById(canvasName);
        this.context = this.canvas.getContext('2d');

        this.scale = 1;

        this.gameLoopIds = [];
        this.onKeyDownListeners = [];
        this.onKeyUpListeners = [];
    }

    getScreenSize() {
        return {
            width: window.innerWidth,
            height: window.innerHeight
        };
    }

    clearCanvas() {
        this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
    }

    calculateScale(width, height, widthGap, heightGap) {
        widthGap = widthGap === undefined ? 0 : widthGap;
        heightGap = heightGap === undefined ? 0 : heightGap;

        let widthScale = 1 + ((this.getScreenSize().width * (1 - widthGap) - width) / width);
        let heightScale = 1 + ((this.getScreenSize().height * (1 - heightGap) - height) / height);

        if (widthScale < heightScale) {
            this.scale = widthScale;
        } else {
            this.scale = heightScale;
        }
    }

    setSize(width, height, calculateScale, widthGap, heightGap) {
        if (calculateScale === true) {
            this.calculateScale(width, height, widthGap, heightGap);
        }

        this.context.canvas.width = width * this.scale;
        this.context.canvas.height = height * this.scale;
    }

    setBackground(color) {
        this.clearCanvas();
        this.canvas.style.backgroundColor = color;
    }

    drawRectangle(x, y, width, height, fillColor, strokeColor, strokeWidth) {
        this.context.fillStyle = fillColor;
        this.context.fillRect(x * this.scale, y * this.scale, width * this.scale, height * this.scale);

        this.context.strokeStyle = strokeColor;
        this.context.lineWidth = strokeWidth;
        this.context.strokeRect(x * this.scale, y * this.scale, width * this.scale, height * this.scale);
    }

    drawRectangleObject(rectangle) {
        if (rectangle instanceof Rectangle) {
            rectangle.draw(this);
        }
    }

    drawImage(rectangle, image) {
        this.context.drawImage(image, rectangle.x * this.scale, rectangle.y * this.scale, rectangle.width * this.scale, rectangle.height * this.scale);
    }

    drawText(text, x, y, color, size, textAlign, font) {
        size = size === undefined ? 100 : size;
        font = font === undefined ? 'Segoe UI' : font;

        this.context.font = size * this.scale + 'px \'' + font + '\'';
        this.context.fillStyle = color === undefined ? 'black' : color;
        this.context.textAlign = textAlign === undefined ? 'center' : textAlign;

        this.context.fillText(text, x * this.scale, y * this.scale);
    }

    onKeyDown(handler, keepOthers) {
        if (keepOthers === undefined || !keepOthers) {
            this.onKeyDownListeners.forEach(function (listener) {
                document.removeEventListener('keydown', listener);
            });
            this.onKeyDownListeners = [];
        }

        let listener = function (event) {
            if (event.defaultPrevented) {
                return;
            }

            if (handler(event)) {
                event.preventDefault();
            }
        };

        this.onKeyDownListeners.push(listener);
        document.addEventListener('keydown', listener);
    }

    onKeyUp(handler, keepOthers) {
        if (keepOthers === undefined || !keepOthers) {
            this.onKeyUpListeners.forEach(function (listener) {
                document.removeEventListener('keyup', listener);
            });
            this.onKeyUpListeners = [];
        }

        let listener = function (event) {
            if (event.defaultPrevented) {
                return;
            }

            if (handler(event)) {
                event.preventDefault();
            }
        };

        this.onKeyUpListeners.push(listener);
        document.addEventListener('keyup', listener);
    }

    setGameLoop(loop, fps, keepOthers) {
        if (keepOthers === undefined || !keepOthers) {
            this.gameLoopIds.forEach(function (id) {
                clearInterval(id);
            });
            this.gameLoopIds = [];
        }

        loop = loop === undefined ? function () {
            } : loop;
        fps = fps === undefined ? 1000 : fps;

        this.gameLoopIds.push(setInterval(loop, 1000 / fps));
    }

}

class ImageLoader {
    constructor(sources) {
        this.sources = sources;
    }

    load(callback) {
        let imagesLoaded = 0;

        for (let source in this.sources) {
            if (this.sources.hasOwnProperty(source)) {
                this[source] = new Image();
                this[source].onload = function () {
                    imagesLoaded++;
                    if (imagesLoaded >= Object.keys(this.sources).length) {
                        callback(this);
                    }
                }.bind(this);
                this[source].src = this.sources[source];
            }
        }
    }
}