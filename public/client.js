'use strict';

let client;
let nextCommand;
let socket;
let gamelib;

window.onload = function () {
    gamelib = new GameLib('canvas');

    const protocol = window.location.protocol === 'https:' ? 'wss' : 'ws';

    socket = new WebSocket(protocol + '://' + window.location.hostname + ':' + window.location.port + '/ws');

    socket.onerror = onConnectionError;
    socket.onclose = loose;
    socket.onmessage = function (rawMessage) {
        const message = JSON.parse(rawMessage.data);

        if (message.loose) {
            loose();
            return;
        }
        if (message.setup) {
            setup(message.setup);
        }
        if (message.updateData) {
            onUpdate(message.updateData);
        }
    };
};

function onConnectionError() {
    // Alert on connection errors
    document.body.style.visibility = 'hidden';
    document.getElementById('canvas').style.visibility = 'visible';

    document.body.style.backgroundColor = 'black';
    document.getElementById('canvas').style.borderColor = 'black';
    document.getElementById('scoreboardTitle').style.color = 'black';
    if (!document.title.startsWith('[error]')) {
        document.title = '[error] ' + document.title;
    }

    gamelib.setGameLoop(function () {
        gamelib.setSize(1920, 1200, true, 0.2, 0.2);
        gamelib.drawText('Conection error!', 1920 / 2, 1200 / 2, 'red', 90, 'center');
        gamelib.drawText('Probably some ports are blocked in your network..', 1920 / 2, 1200 / 2 + 80, 'red', 50, 'center');
        gamelib.drawText('(press any key to try again)', 1920 / 2, 1200 / 2 + 140, 'red', 50, 'center');
    });

    gamelib.onKeyDown(function () {
        location.reload();
    });
}

function setup(userId) {
    // Set the body visible when the loading is complete
    document.body.style.visibility = 'visible';

    client = new Client(userId);

    gamelib.setGameLoop(function () {
        if (client !== undefined && client.snake !== undefined) {
            client.draw();
        }
    });

    gamelib.onKeyDown(function (event) {
        switch (event.key) {
            case 'ArrowDown':
            case 's':
                nextCommand = 'DOWN';
                return true;
            case 'ArrowUp':
            case 'w':
                nextCommand = 'UP';
                return true;
            case 'ArrowLeft':
            case 'a':
                nextCommand = 'LEFT';
                return true;
            case 'ArrowRight':
            case 'd':
                nextCommand = 'RIGHT';
                return true;
            default:
                return false;
        }
    });
}

function onUpdate(updateData) {
    // Set all the updated data
    client.updateData = updateData;

    client.canvas = updateData.canvas;

    client.otherUsers = updateData.users.filter(function (user) {
        return user.snake.direction !== undefined && user.id !== client.userId;
    });

    let thisUser = updateData.users.find(function (user) {
        return user.id === client.userId;
    });

    if (thisUser !== undefined) {
        thisUser.snake.rectangles[0].fillColor = 'white';
        client.snake = thisUser.snake;
        client.setColorStreak(thisUser.colorStreak);
        client.score = thisUser.score;
    }

    client.collectibles = updateData.collectibles;

    // Display the score board
    client.setScoreBoard(updateData.users);

    // Send the next command
    socket.send(JSON.stringify({
        command: nextCommand
    }));
}

function loose() {
    socket.close();

    document.body.style.backgroundColor = client.canvas.color;
    document.getElementById('canvas').style.borderColor = client.canvas.color;
    document.getElementById('scoreboardTitle').style.color = 'white';
    document.title = '[lost] ' + document.title;

    gamelib.setGameLoop(function () {
        client.draw();
        gamelib.drawText('You lost.', client.canvas.width / 2, client.canvas.height / 2, 'red', 90, 'center');
        gamelib.drawText('Score: ' + client.score + ' - Max Streak: ' + client.colorStreak.maxStreak, client.canvas.width / 2, client.canvas.height / 2 + 90, 'red', 90, 'center');
        gamelib.drawText('(press any key to continue)', client.canvas.width / 2, client.canvas.height / 2 + 150, 'red', 50, 'center');
    });

    gamelib.onKeyDown(function () {
        location.reload();
    });
}

class Client {
    constructor(userId) {
        this.userId = userId;

        this.snake = undefined;
        this.canvas = undefined;
        this.otherUsers = [];
        this.collectibles = [];

        this.score = undefined;
        this.colorStreak = undefined;

        this.updateData = undefined;
    }


    setColorStreak(colorStreak) {
        this.colorStreak = colorStreak;
        // Set the canvas border
        document.getElementById('canvas').style.borderColor = colorStreak.color;
        document.getElementById('scoreboardTitle').style.color = colorStreak.color;
    }

    draw() {
        // Draw the canvas (and calculate scale
        gamelib.setSize(this.canvas.width, this.canvas.height, true, 0.2, 0.2);
        gamelib.setBackground(this.canvas.color);

        // Draw other snakes
        this.otherUsers.forEach(function (user) {
            user.snake.rectangles.forEach(function (rectangles) {
                this.drawRectangle(rectangles, 1);
            }, this);
        }, this);

        // Draw the main snake
        this.snake.rectangles.forEach(function (rectangle) {
            this.drawRectangle(rectangle, 1);
        }, this);

        // Draw collectibles
        this.collectibles.forEach(function (collectible) {
            this.drawRectangle(collectible);
        }, this);
    }

    drawRectangle(rectangle, gap) {
        gap = gap === undefined ? 0 : gap;

        // Draw a rectangle with gaps provided
        gamelib.drawRectangle(
            (rectangle.x + gap),
            (rectangle.y + gap),
            (rectangle.width - gap * 2),
            (rectangle.height - gap * 2),
            rectangle.fillColor,
            rectangle.strokeColor,
            rectangle.strokeWidth
        );
    }

    setScoreBoard(users) {
        users.sort(function (a, b) {
            return b.score - a.score;
        });

        let scoreboard = document.getElementById('scoreboard');
        scoreboard.innerHTML = '';

        for (let i = 0; i < users.length; i++) {
            let row = scoreboard.insertRow();
            row.style.color = users[i].snake.rectangles[0].strokeColor;

            // Append cell to display color and id
            let userCell = row.insertCell();
            // Append color name
            userCell.appendChild(document.createTextNode(users[i].snake.rectangles[0].strokeColor));
            // Append id
            let userIdSub = document.createElement('sub');
            userIdSub.appendChild(document.createTextNode(users[i].id));
            userCell.appendChild(userIdSub);
            // Append colon
            userCell.appendChild(document.createTextNode(':'));

            // Append Score
            row.insertCell().appendChild(
                document.createTextNode(
                    users[i].score
                ));

            // Append streak
            row.insertCell().appendChild(
                document.createTextNode(
                    (users[i].colorStreak.currentStreak > 1 ? 'x' + users[i].colorStreak.currentStreak + '' : '')
                ));

            // Append self indicator
            row.insertCell().appendChild(
                document.createTextNode(
                    (this.userId === users[i].id ? '<- you' : '')
                ));
        }
    }
}