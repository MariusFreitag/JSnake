'use strict';

const Rectangle = require('./rectangle.js');
const c = require('./constants.js');

class Snake {
    constructor(x, y, fillColor, strokeColor) {
        this.rectangles = [new Rectangle(x, y, c.SNAKE_SIZE, c.SNAKE_SIZE, fillColor, strokeColor, 3)];

        this.direction = undefined;
    }

    setDirection(direction) {
        let directions = ['UP', 'DOWN', 'RIGHT', 'LEFT'];
        if (!directions.includes(direction) ||
            (this.rectangles.length > 1 &&
            (this.direction === 'RIGHT' && direction === 'LEFT' ||
            this.direction === 'UP' && direction === 'DOWN' ||
            this.direction === 'LEFT' && direction === 'RIGHT' ||
            this.direction === 'DOWN' && direction === 'UP'))) {
            return;
        }

        this.direction = direction;
    }

    setPosition(x, y) {
        for (let i = this.rectangles.length - 1; i > 0; i--) {
            this.rectangles[i].x = this.rectangles[i - 1].x;
            this.rectangles[i].y = this.rectangles[i - 1].y;
        }

        this.rectangles[0].x = x;
        this.rectangles[0].y = y;
    }

    move() {
        let x = this.rectangles[0].x;
        let y = this.rectangles[0].y;
        switch (this.direction) {
            case 'RIGHT':
                x += c.SNAKE_SIZE;
                break;

            case 'LEFT':
                x -= c.SNAKE_SIZE;
                break;

            case 'UP':
                y -= c.SNAKE_SIZE;
                break;

            case 'DOWN':
                y += c.SNAKE_SIZE;
                break;
        }
        this.setPosition(x, y);
    }

    collidesSelf() {
        for (let i = 2; i < this.rectangles.length; i++) {
            if (this.rectangles[0].doesCollide(this.rectangles[i])) {
                return true;
            }
        }
        return false;
    }

    collidesWithOther(otherSnake) {
        if (otherSnake.direction === undefined ||
            this.direction === undefined) {
            return false;
        }

        for (let i = 0; i < otherSnake.rectangles.length; i++) {
            if (this.rectangles[0].doesCollide(otherSnake.rectangles[i])) {
                return true;
            }
        }
        return false;
    }

    addSuccessor(fillColor) {
        let x = this.rectangles[0].x;
        let y = this.rectangles[0].y;

        switch (this.direction) {
            case 'RIGHT':
                x -= this.rectangles[0].width;
                break;

            case 'LEFT':
                x += this.rectangles[0].width;
                break;

            case 'UP':
                y += this.rectangles[0].height;
                break;

            case 'DOWN':
                y -= this.rectangles[0].height;
                break;
        }
        this.rectangles.push(new Rectangle(x, y, this.rectangles[0].width, this.rectangles[0].height, fillColor, this.rectangles[0].strokeColor, this.rectangles[0].strokeWidth));

    }

    getObject() {
        let rectangleDataObjects = [];
        this.rectangles.forEach(function (rectangle) {
            rectangleDataObjects.push(rectangle.getObject());
        });

        return {
            rectangles: rectangleDataObjects,
            direction: this.direction
        };
    }
}

module.exports = Snake;
