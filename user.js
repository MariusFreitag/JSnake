'use strict';

class User {
    constructor(id, snake, socket, server) {
        this.id = id;

        this.snake = snake;
        this.score = 0;
        this.colorStreak = { color: 'white', currentStreak: 0, maxStreak: 0 };

        this.socket = socket;
        this.server = server;

        this.lost = false;

        this.registerSocketListeners();
        this.reAdd();
        this.sendSetup();
    }

    registerSocketListeners() {
        this.socket.on('close', function () {
            this.removeFrom(this.server.users);
            console.log('User ' + this.id + ' disconnected (' + this.server.users.length + ' users connected)');
        }.bind(this));

        this.socket.on('message', function (rawMessage) {
            const message = JSON.parse(rawMessage);
            if (message.command) {
                this.snake.setDirection(message.command);
                this.reAdd();
            }
        }.bind(this));
    }

    sendSetup() {
        this.socket.send(JSON.stringify({
            setup: this.id
        }));
        console.log('User ' + this.id + ' connected (' + this.server.users.length + ' users connected)');
    }

    loose() {
        this.lost = true;
        this.socket.send(JSON.stringify({
            loose: true
        }));
        this.socket.close();
    }

    increaseScore(amount) {
        this.score += amount;
    }

    reAdd() {
        this.removeFrom(this.server.users);
        this.server.users.push(this);
    }

    removeFrom(array) {
        for (let i = 0; i < array.length; i++) {
            if (array[i].id === this.id) {
                array.splice(i, 1);
            }
        }
    }

    collectibleCollision(collectible) {
        // Color streak handling
        if (this.colorStreak.color !== collectible.fillColor) {
            this.colorStreak.color = collectible.fillColor;
            this.colorStreak.currentStreak = 1;
        } else {
            this.colorStreak.currentStreak++;
            if (this.colorStreak.currentStreak > this.colorStreak.maxStreak) {
                this.colorStreak.maxStreak = this.colorStreak.currentStreak;
            }
        }
        this.increaseScore(this.colorStreak.currentStreak);

        for (let i = 0; i < this.colorStreak.currentStreak; i++) {
            this.snake.addSuccessor(collectible.fillColor);
        }
    }
}

module.exports = User;