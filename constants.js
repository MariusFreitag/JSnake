'use strict';

module.exports = {
    COLLECTIBLES_COUNT: 10,
    SNAKE_SIZE: 20,
    CANVAS_WIDTH: 1920 * 0.75,
    CANVAS_HEIGHT: 1080 * 0.75
};